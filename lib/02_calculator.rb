def add(x, y)
  x + y
end

def subtract(x, y)
  x - y
end

def sum(numbers)
  sum = 0
  numbers.each { |number| sum += number }
  sum
end
